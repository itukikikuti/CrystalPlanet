﻿using UnityEngine;
using System.Collections;

public class PlayerAngle : MonoBehaviour
{
    public Vector2 speed;
    private Rigidbody parentRigidbody;
    private Vector3 tempRotation;

    void Start()
    {
        parentRigidbody = transform.parent.GetComponent<Rigidbody>();
    }

    void Update()
    {
        //tempRotation.x++;
        transform.rotation = Quaternion.Euler(tempRotation.x, transform.rotation.y, transform.rotation.z);
        parentRigidbody.angularVelocity = new Vector3(0.0f, Input.GetAxis("Mouse X") * speed.x, 0.0f);
    }
}
