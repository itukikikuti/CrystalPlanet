﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // キー入力で任意の方向に加速する
        if (Input.GetKey(KeyCode.W))
            rb.velocity += transform.forward;

        if (Input.GetKey(KeyCode.S))
            rb.velocity -= transform.forward;

        if (Input.GetKey(KeyCode.A))
            rb.velocity -= transform.right;

        if (Input.GetKey(KeyCode.D))
            rb.velocity += transform.right;

        rb.velocity *= 0.9f;
    }
}
